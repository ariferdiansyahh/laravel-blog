<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use Illuminate\Support\Carbon;
class PostController extends Controller
{
    public function index(){
        $posts = Post::with(['detail', 'author'])->get();

        $response = [
            'data' => [
                'postLists' => $posts->map(function ($post) {
                    $startDate = Carbon::parse($post->detail->start_date)->format('M j, Y');
                    $endDate = Carbon::parse($post->detail->end_date)->format('M j, Y');
                    return [
                        'id'       => $post->id,
                        'title'    => $post->title,
                        'category' => $post->category,
                        'image'    => $post->image,
                        'slug'     => $post->slug,
                        'detail'   => [
                            'date' => "{$startDate} - {$endDate}",
                            'time' => '08:00 - 21:00',
                            'desc' => $post->detail->description,
                            'tags' => $post->detail->tags,
                        ],
                        'author' => [
                            'name' => $post->author->name,
                            'uuid' => $post->author->id,
                        ],
                    ];
                })->toArray(),
            ],
        ];

        return response()->json($response);
    }
}
