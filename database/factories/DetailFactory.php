<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\PostDetail>
 */
class DetailFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'start_date' => $this->faker->dateTimeBetween('-1 month', 'now'),
            'end_date' => $this->faker->dateTimeBetween('now', '+1 month'),
            'description' => $this->faker->paragraph,
            'tags' => $this->faker->words(5), // Menghasilkan 5 kata acak dan menggabungkannya dengan koma
            'post_id' => \App\Models\Post::factory(),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
