<?php

namespace Database\Factories;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Post>
 */
class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'id' => $this->faker->uuid,
            'title' => $this->faker->sentence, // Menghasilkan kalimat acak
            'category' => $this->faker->word, // Menghasilkan kata acak
            'image' => $this->faker->imageUrl(), // Menghasilkan URL gambar acak
            'slug' => $this->faker->slug, // Menghasilkan slug acak
            'user_id' => User::all()->random(),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
