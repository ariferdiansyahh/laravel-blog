# Dokumentasi API JWT Auth untuk Laravel Blog

## Login Pengguna
Gunakan kredensial berikut untuk login melalui API:

- Email: `test@example.com`
- Password: `password`

## Dokumentasi Postman
Lihat dokumentasi Postman lengkap untuk API ini di [sini](https://documenter.getpostman.com/view/29421533/2sA2rCTLsL).

## Situs Web
Akses Laravel Blog melalui link berikut: [http://103.18.76.93:8888/login](http://103.18.76.93:8888/login)
