<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Home Page - List Blog</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <style>
        .logout-button {
            position: fixed;
            right: 20px;
            top: 20px;
        }
    </style>
</head>
<body>

<div class="logout-button">
    <button id="logoutBtn" class="btn btn-danger">Logout</button>
</div>

<div class="container mt-5">
    <h2>List Blog</h2>
    <div id="blogList" class="row"></div>
</div>

<!-- Detail Modal -->
<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="detailModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="detailModalLabel">Blog Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Detail Description -->
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>

<script>
    $(document).ready(function() {
        $.ajax({
            url: 'api/post/auth/get',
            type: 'GET',
            dataType: 'json',
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('jwt_token')
            },
            success: function(response) {
                var posts = response.data.postLists;
                $.each(posts, function(index, post) {
                    // Mengonversi tags array menjadi string yang dipisahkan koma
                    var tags = post.detail.tags.join(', ');

                    // Menambahkan post ke dalam blogList
                    $('#blogList').append(
                        `<div class="col-md-6 mb-4">
                            <div class="card h-100">
                                <img src="${post.image}" class="card-img-top" alt="${post.title}">
                                <div class="card-body">
                                    <h5 class="card-title">${post.title}</h5>
                                    <p class="card-text"><strong>Category:</strong> ${post.category}</p>
                                    <p class="card-text"><strong>Date:</strong> ${post.detail.date}</p>
                                    <p class="card-text"><strong>Tags:</strong> ${tags}</p>
                                    <p class="card-text"><strong>Author:</strong> ${post.author.name}</p>
                                    <button class="btn btn-primary detail-btn" data-desc="${post.detail.desc}"><i class="fas fa-eye"></i> Detail</button>
                                </div>
                                <div class="card-footer">
                                    <small class="text-muted">Last updated 3 mins ago (Dummy)</small>
                                </div>
                            </div>
                        </div>`
                    );
                });
            },
            error: function(xhr, status, error) {
                // Handle error
                if (xhr.status == 401) { // Unauthorized
                    localStorage.removeItem('jwt_token');
                    window.location.href = 'login';
                }
            }
        });

        // Event listener for detail button
        $(document).on('click', '.detail-btn', function() {
            var desc = $(this).data('desc');
            var title = $(this).closest('.card-body').find('.card-title').text();
            $('#detailModal .modal-body').html(desc);
            $('#detailModalLabel').text(title);
            $('#detailModal').modal('show');
        });
        $('#logoutBtn').click(function() {
            localStorage.removeItem('jwt_token'); // Remove the token
            window.location.href = 'login'; // Redirect to login page
        });
    });
</script>
</body>
</html>
